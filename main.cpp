/**
 *   @author Ivo Strejc
 */
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <openssl/evp.h>
#include <openssl/rand.h>
#include <openssl/pem.h>

#define HEADER_SIZE_BYTE_LENGTH 4
#define MAX_LOADED 1024
using namespace std;

class fstreamException {
public:

    fstreamException(string mes) : message(mes) {
    }

    string getMessage() {
        return message;
    }
private:
    string message;
};

unsigned char getCharSafe(ifstream & fs) {
    unsigned char tmp = fs.get();
    if (fs.fail()) throw fstreamException("Chyba souboru.");
    return tmp;
}

size_t loadNBytesAsNumber(ifstream & fs, size_t n) {
    size_t tmp = 0;
    unsigned char wrk;
    for (size_t i = 0; i < n; i++) {
        wrk = getCharSafe(fs);
        tmp += (wrk * pow(2, i * 8));
    }
    return tmp;
}

size_t loadNBytesIntoArray(ifstream & fs, size_t n, unsigned char arr[MAX_LOADED]) {
    size_t i;
    try {
        for (i = 0; i < n; i++) {
            arr[i] = getCharSafe(fs);
        }
    } catch (fstreamException ex) {
    }
    return i;
}

bool writeNBytes(ofstream & os, size_t n, unsigned char * data) {
    for (size_t i = 0; i < n; i++) os.put(data[i]);
    os.flush();
    return os.good();
}

bool writeNumberAsNBytes(ofstream& os, size_t n, int x) {
    for (size_t i = 0; i < n; i++) os.put((x >> (i * 8))&0xff);
    os.flush();
    return os.good();
}

bool loadHeader(ifstream& inF, string& cipher_name, unsigned char iv[EVP_MAX_IV_LENGTH], string& key){
    try{
        size_t header_size = loadNBytesAsNumber(inF, HEADER_SIZE_BYTE_LENGTH);
        size_t cipher_name_size = loadNBytesAsNumber(inF, 1);
        cipher_name = "";
        for(size_t i = 0; i < cipher_name_size; i++){
            cipher_name += getCharSafe(inF);
        }
        loadNBytesIntoArray(inF, EVP_MAX_IV_LENGTH, iv);
        key = "";
        for(size_t i = cipher_name_size + 1 + EVP_MAX_IV_LENGTH; i < header_size; i++){
            key += getCharSafe(inF);
        }
    } catch (fstreamException ex){
        cout<<"Chyba načítání hlavičky."<<endl;
        return false;
    }
    return true;
}
/*void skipNBytes(ifstream & fs, size_t n){
    for(size_t i = 0; i < n; i++){
        getCharSafe(fs);
    }
}

bool loadHeader(ifstream & fs, size_t & fsSize, size_t & headerSize, bool checkForLen = true){
    unsigned char wrk = getCharSafe(fs);
    if (wrk != 'B') return false;
    wrk = getCharSafe(fs);
    if (wrk != 'M') return false;
    fsSize = loadNBytesAsNumber(fs, 4);
    if(checkForLen){
        ifstream::pos_type temp = fs.tellg();
        skipNBytes(fs, fsSize - 6);
        if(fs.get() != EOF) return false;
        fs.clear();
        fs.seekg(temp);
    }
    skipNBytes(fs, 4);
    size_t dataBegin = loadNBytesAsNumber(fs, 4);
    //cout<<dataBegin<<" "<<fsSize<<endl;
    headerSize = dataBegin;
    skipNBytes(fs, headerSize - 14);
    return true;
}

string getExtension(string fileName){
    string tmp = "";
    string tmpName(fileName);
    reverse (tmpName.begin(), tmpName.end());
    bool found = false;
    for(size_t i = 0; i < tmpName.size(); i++){
        tmp+= tmpName[i];
        if(tmpName[i] == '.'){
            found = true;
            break;
        }
    }
    if(found){
        reverse(tmp.begin(), tmp.end());
        return tmp;
    } return "";
}

bool copyFirstNBytes(ifstream & fs, ofstream & of, size_t n){
    fs.seekg(fs.beg);
    for(size_t i = 0; i < n; i++){
        of.put(getCharSafe(fs));
    }
    of.flush();
    return of.good();
}

bool crypt(ifstream & fs, ofstream & of, const EVP_CIPHER * cipher, unsigned char * key, unsigned char * iv, int do_encypt){
    EVP_CIPHER_CTX ctx;
    EVP_CipherInit(&ctx, cipher, key, iv, do_encypt);
    unsigned char inBuff[maxLoaded];
    unsigned char outBuff[maxLoaded + EVP_MAX_BLOCK_LENGTH];
    size_t read = 0;
    int crypted;
    while((read = loadNBytesIntoArray(fs, maxLoaded, inBuff))){
        if(!EVP_CipherUpdate(&ctx, outBuff, &crypted, inBuff, read)){
            EVP_CIPHER_CTX_cleanup(&ctx);
            return false;
        }
        if(!writeNBytes(of, crypted, outBuff)){
            cout<<"Chyba zapisu."<<endl;
            EVP_CIPHER_CTX_cleanup(&ctx);
            return false;
        }
    }
    if(!EVP_CipherFinal(&ctx, outBuff, &crypted)){
        EVP_CIPHER_CTX_cleanup(&ctx);
        return false;
    }
    if(!writeNBytes(of, crypted, outBuff)){
        cout<<"Chyba zapisu."<<endl;
        EVP_CIPHER_CTX_cleanup(&ctx);
        return false;
    }
    EVP_CIPHER_CTX_cleanup(&ctx);
    return true;
}



bool cipherAndSaveHeader(unsigned char * header, size_t header_size, EVP_PKEY * key, ofstream & of) {
    EVP_PKEY_CTX *ctx;
    unsigned char *out;
    size_t out_len;
    ctx = EVP_PKEY_CTX_new(key, NULL);
    if(!ctx || EVP_PKEY_encrypt_init(ctx) <= 0){
        cout<<"Chyba pri inicializaci sifrovaci RSA."<<endl;
        return false;
    }
    if (EVP_PKEY_encrypt(ctx, NULL, &out_len, header, header_size) <= 0){
        cout<<"Chyba při šifrování RSA."<<endl;
        return false;
    }
    if (!(out = OPENSSL_malloc(out_len))){
        cout<<"Chyba mallocu pro výstup RSA."<<endl;
        return false;
    }
    if (EVP_PKEY_encrypt(ctx, out, &out_len, header, header_size) <= 0){
        cout<<"Chyba při šifrování RSA."<<endl;
        return false;
    }
    try{
        writeNBytes(of, out_len, out);
    } catch (fstreamException ex){
        cout<<"Chyba zápisu hlavičky."<<endl;
        return false;
    }
    return true;
}
 */

int main(int argc, char** argv) {

    enum operation {
        DECRYPT, ENCRYPT, OP_NA
    };
    string input_file_name = "", output_file_name = "", key_file_name = "", sym_cipher_name = "";
    operation what_to_do = OP_NA; //zatim na nic
    if (argc < 4) {
        cout << "Nedostatek argumentů, spusťte s přepínačem -h pro nápovědu." << endl;
        return 1;
    }
    if (argc > 6) {
        cout << "Moc argumentů, spusťte s přepínačem -h pro nápovědu." << endl;
        return 1;
    } else {
        for (int i = 1; i < argc; i++) {
            string wrkTmp(argv[i]);
            if (wrkTmp[0] == '-') {
                //Prepinac
                if (wrkTmp == "-h") {
                    cout << "Pro šifrování dejte prepinac -e (je také defaultně nastaven), pro dešifrování dejte přepínač -d. "
                            << "Následuje cesta k souboru ke klíči, cesta ke vstupnímu souboru a cesta k výstupnímu souboru. Volitelný je pro šifrování zvolení symtrické šifry" << endl;
                    return 0;
                }
                if (wrkTmp == "-e") {
                    if (what_to_do != OP_NA) {
                        cout << "Bylo vybráno moc přepínačů pro operační mód, spusťte s přepínačem -h pro nápovědu." << endl;
                        return 1;
                    }
                    what_to_do = ENCRYPT;
                } else if (wrkTmp == "-d") {
                    if (what_to_do != OP_NA) {
                        cout << "Bylo vybráno moc přepínačů pro operační mód, spusťte s přepínačem -h pro nápovědu." << endl;
                        return 1;
                    }
                    what_to_do = DECRYPT;
                } else {
                    cout << "Neplatný přepínač, spusťte s přepínačem -h pro nápovědu." << endl;
                    return 1;
                }
            } else {
                if (key_file_name == "") key_file_name = wrkTmp;
                else if (input_file_name == "") input_file_name = wrkTmp;
                else if (output_file_name == "") output_file_name = wrkTmp;
                else if (sym_cipher_name == "") sym_cipher_name = wrkTmp;
                else {
                    //Jmeno souboru jiz bylo vlozeno
                    cout << "Zadáno více jmen souborů, zadejte správný počet." << endl;
                    return 1;
                }
            }
        }
    }
    if (input_file_name == "" || key_file_name == "" || output_file_name == "") {
        cout << "Nebylo zadána jména všech souborů, spusťte s přepínačem -h pro nápovědu." << endl;
        return 1;
    }
    if (what_to_do == OP_NA) {
        cout << "Nebyla zadána operace, předpokládám encryptování." << endl;
        what_to_do = ENCRYPT;
    }
    if (sym_cipher_name != "" && what_to_do != ENCRYPT) {
        cout << "Bylo zadáno parametrem jméno symetrické šifry při dešifrování, ignoruji" << endl;
    } else if (sym_cipher_name == "" && what_to_do == ENCRYPT) {
        cout<<"Nastavuji sifru na aes-256-cbc"<<endl;
        sym_cipher_name = "aes-256-cbc";
    }
    
    FILE * keyF = fopen(key_file_name.c_str(), "r");
    if (!keyF) {
        cout << "Chyba otevření souboru s klíčem." << endl;
        return 1;
    }
    ifstream inF(input_file_name, fstream::in | fstream::binary);
    if (!inF.is_open()) {
        cout << "Chyba otevření vstupního souboru." << endl;
        return 1;
    }
    ofstream outF(output_file_name, fstream::out | fstream::binary);
    if (!outF.is_open()) {
        cout << "Chyba otevření výstupního souboru." << endl;
        return 1;
    }
    const EVP_CIPHER * cipher;
    OpenSSL_add_all_ciphers();
    EVP_CIPHER_CTX ctx;
    if (what_to_do == ENCRYPT) {
        if (RAND_load_file("/dev/random", 32) != 32) {
            cout << "Cannot seed the random generator!" << endl;
            return 1;
        }
        cipher = EVP_get_cipherbyname(sym_cipher_name.c_str());
        if (!cipher) {
            cout << "Sifra " << sym_cipher_name << " neexistuje, ukončuji" << endl;
            return 1;
        }
        EVP_PKEY * pubkey;
        if (!(pubkey = PEM_read_PUBKEY(keyF, NULL, NULL, NULL))) {
            cout << "Nepovedlo se načíst veřejný klíč" << endl;
            return 1;
        }
        unsigned char * generated_key = new unsigned char[EVP_PKEY_size(pubkey)]; // alokace dostatečně velkého prostoru pro klíč, který mi vrátí SealInit
        int generated_key_len = 0; // delka sym klice, ktery mi vrati SealInit
        unsigned char iv[EVP_MAX_IV_LENGTH]; // vzdy bude maximálně takto dlouhý
        
        if (!EVP_SealInit(&ctx, cipher, &generated_key, &generated_key_len, iv, &pubkey, 1)) {
            cout << "Chyba při SealInit." << endl;
            delete [] generated_key;
            //free(pubkey); // Nejsem schopen najít správný postup pro uvolnění paměti PKEY...
            EVP_CIPHER_CTX_cleanup(&ctx);
        }

        /* Vytvoření nezašifrované hlavičky */
        size_t header_size = 1 /* prvni bit pro delku nazvu sym. sifry*/ + sym_cipher_name.length() + EVP_MAX_IV_LENGTH + generated_key_len;
        unsigned char * header = new unsigned char [header_size];
        header[0] = (sym_cipher_name.length()&0xff);
        memcpy(header + 1, sym_cipher_name.c_str(), sym_cipher_name.length());
        memcpy(header + 1 + sym_cipher_name.length(), iv, EVP_MAX_IV_LENGTH);
        memcpy(header + 1 + sym_cipher_name.length() + EVP_MAX_IV_LENGTH, generated_key, generated_key_len);

        try {
            writeNumberAsNBytes(outF, HEADER_SIZE_BYTE_LENGTH, header_size);
            writeNBytes(outF, header_size, header);
        } catch (fstreamException ex) {
            cout << "Chyba zápisu hlavičky." << endl;
            return 1;
        }
        delete [] generated_key;
        unsigned char inBuff[MAX_LOADED];
        unsigned char outBuff[MAX_LOADED + EVP_MAX_BLOCK_LENGTH];
        size_t read = 0;
        int crypted;
        while ((read = loadNBytesIntoArray(inF, MAX_LOADED, inBuff))) {
            if (!EVP_SealUpdate(&ctx, outBuff, &crypted, inBuff, read)) {
                EVP_CIPHER_CTX_cleanup(&ctx);
                return 1;
            }
            if (!writeNBytes(outF, crypted, outBuff)) {
                cout << "Chyba zapisu." << endl;
                EVP_CIPHER_CTX_cleanup(&ctx);
                return 1;
            }
        }
        if (!EVP_SealFinal(&ctx, outBuff, &crypted)) {
            EVP_CIPHER_CTX_cleanup(&ctx);
            return 1;
        }
        if (!writeNBytes(outF, crypted, outBuff)) {
            cout << "Chyba zapisu." << endl;
            EVP_CIPHER_CTX_cleanup(&ctx);
            return 1;
        }
        EVP_CIPHER_CTX_cleanup(&ctx);

        //free(pubkey); // Nejsem schopen najít správný postup pro uvolnění paměti PKEY...
    }
    else{
        string cipher_name = "", key = "";
        unsigned char iv[EVP_MAX_IV_LENGTH];
        if (!loadHeader(inF, cipher_name, iv, key)) return 1;
        cipher = EVP_get_cipherbyname(cipher_name.c_str());
        if (!cipher) {
            cout << "Sifra " << cipher_name << " neexistuje, ukončuji" << endl;
            return 1;
        }
        EVP_PKEY * private_key;
        if (!(private_key = PEM_read_PrivateKey(keyF, NULL, NULL, NULL))) {
            cout << "Nepovedlo se načíst privátní klíč" << endl;
            return 1;
        }
        if(!EVP_OpenInit(&ctx, cipher, (unsigned char *)key.c_str(), key.length(), iv, private_key)){
            cout<<"Nepodařil se OpenInit."<<endl;
            return 1;
        }
        unsigned char inBuff[MAX_LOADED];
        unsigned char outBuff[MAX_LOADED + EVP_MAX_BLOCK_LENGTH];
        size_t read = 0;
        int crypted;
        while ((read = loadNBytesIntoArray(inF, MAX_LOADED, inBuff))) {
            if (!EVP_OpenUpdate(&ctx, outBuff, &crypted, inBuff, read)) {
                EVP_CIPHER_CTX_cleanup(&ctx);
                return 1;
            }
            if (!writeNBytes(outF, crypted, outBuff)) {
                cout << "Chyba zapisu." << endl;
                EVP_CIPHER_CTX_cleanup(&ctx);
                return 1;
            }
        }
        if (!EVP_OpenFinal(&ctx, outBuff, &crypted)) {
            EVP_CIPHER_CTX_cleanup(&ctx);
            return 1;
        }
        if (!writeNBytes(outF, crypted, outBuff)) {
            cout << "Chyba zapisu." << endl;
            EVP_CIPHER_CTX_cleanup(&ctx);
            return 1;
        }
        EVP_CIPHER_CTX_cleanup(&ctx);

    }
    cout << "Vse probehlo v poradku." << endl;
    return 0;
}
